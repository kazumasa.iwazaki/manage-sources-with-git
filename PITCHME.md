### 価値あるサービスを提供したい

---

### 実現するためには
</br>
質の高いプログラムの作成が必須です

---

### 現実はこんな問題が．．．
</br>
* 先祖返り
* 開発作業と並行した緊急対応
* 複数人で並行開発したときのマージ
* etc.

---

### 問題解決のために
</br>
「Ｇｉｔ」の使用を提案します

---

### Ｇｉｔとは？？？
</br>
* バージョン管理システムの一つ
* Linuxのカーネルのソース管理のために作成された
* 分散型バージョン管理システム

+++

http://www.atmarkit.co.jp/ait/articles/1307/05/news028.html

---

### Git-flowが非常に便利
ブランチモデルにより柔軟に対応が可能です

---

![git-model](images/git-model@2x_small.png)
</br>
「[A successful Git branching model](https://nvie.com/posts/a-successful-git-branching-model/)」より引用

+++

![git-model](images/git-model@2x.png)

---

### 実際の操作をお見せします

+++

* featureブランチを切って開発
* hotfixブランチを切って緊急対応
* releaseブランチを切って現地適用準備

---

### 使用前の準備（インストール作業）
</br>
* Ｇｉｔクライアント
* SourceTree

---

### 管理ツールを使いませんか？
